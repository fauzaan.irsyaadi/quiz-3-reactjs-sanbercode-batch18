import React from 'react'
import './public/css/style.css'

function About(){
    return(
        <>
            <div>
                <section>
                    <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                    <ol>
                        <li><strong width= "100px">Nama:</strong> Muhammad Fauzaan Irsyaadi</li> 
                        <li><strong width= "100px">Email:</strong> fauzaan.irsyaadi@gmail.com</li> 
                        <li><strong width= "100px">Sistem Operasi yang digunakan:</strong> Windows 10</li>
                        <li><strong width= "100px">Akun Gitlab:</strong> @fauzaanirsyaadi</li> 
                        <li><strong width= "100px">Akun Telegram:</strong> @fauzaanirsyaadi</li> 
                    </ol>
                </section>
            </div>
        </>
    )
}

export default About